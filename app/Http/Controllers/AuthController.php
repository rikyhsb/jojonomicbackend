<?php

namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Laravel\Socialite\Facades\Socialite as Socialite;

class AuthController extends Controller
{

    /**
     * Redirect user to the Facebook authentication page.
     *
     * @return Response
     */
    public function redirect() {
        return Socialite::driver('facebook')->stateless()->redirect();
    }

    /**
     * Obtain the user information from Facebook.
     *
     * @return JsonResponse
     */
    public function callback() {
        $providerUser = Socialite::driver('facebook')->stateless()->user();
        return new JsonResponse($providerUser);
    }
}
